import { rest } from 'msw'
import { setupServer } from 'msw/node'

import { followersHandlers, unfollowersHandlers } from '../mocks/followers'
import { tweetsHandlers } from '../mocks/tweets'
import { usersHandlers } from '../mocks/users'

const server = setupServer(...followersHandlers, ...unfollowersHandlers, ...usersHandlers, ...tweetsHandlers)
beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

export { rest, server }
