import React from 'react'

import { MainLayout } from '@/ui/layout/main.layout'

import AppProvider from './common/contexts/app.context'

// Start Mock Server in DEV
if (process.env.VITE_ENVIRONMENT === 'development') {
  const { server } = require('./mocks/workerSetup')
  server.start()
}

const App = (): JSX.Element => {
  return (
    <AppProvider>
      <MainLayout />
    </AppProvider>
  )
}

export default App
