import { setupWorker } from 'msw'

import { followersHandlers, unfollowersHandlers } from './followers'
import { tweetsHandlers } from './tweets'
import { usersHandlers } from './users'

export const server = setupWorker(...followersHandlers, ...unfollowersHandlers, ...usersHandlers, ...tweetsHandlers)
