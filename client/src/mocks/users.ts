import { rest } from 'msw'

import { FAKE_USERS } from '@/common/testing/mocks/users'
const API_URL = process.env.VITE_SSR_API_HOST


export const usersHandlers = [
  rest.get(`${API_URL}api/users`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(FAKE_USERS)
    )
  })

]
