import { rest } from 'msw'

const API_URL = process.env.VITE_SSR_API_HOST


export const tweetsHandlers = [
  rest.post(`${API_URL}api/tweets`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        data: {
          message: 'test',
          author_id: '617006a4f20e84e76bb11e78',
          published_date: '2021-10-26T08:13:54.949Z',
          _id: '6177b8c2c3c969958179e845',
          __v: 0
        },
        message: 'Tweet added succesfully'
      })
    )
  }),
  rest.get(`${API_URL}api/tweets`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([
        {
          _id: '6176906e92523ea5c9d8ce81',
          author_id: '61700708f20e84e76bb11e7d',
          message: 'I love Jim',
          published_date: '2021-09-10T09:09:09.716Z'
        },
        {
          _id: '61768479ac45242f04b0873c',
          message: 'Jim is the best',
          author_id: '617006ccf20e84e76bb11e7b',
          published_date: '2021-10-14T09:51:07.264Z'
        },
        {
          _id: '61767ec5ac45242f04b0873b',
          message: 'Jim sucks',
          author_id: '61700725f20e84e76bb11e81',
          published_date: '2021-10-15T09:46:09.716Z'
        },
        {
          _id: '61767e3fac45242f04b0873a',
          message: 'From dunder mifflin',
          author_id: '617006ccf20e84e76bb11e7b',
          published_date: '2021-10-24T09:51:07.264Z'
        },
        {
          _id: '61768fe76755c4577fb943ec',
          message: 'hello twitter',
          author_id: '617006a4f20e84e76bb11e78',
          published_date: '2021-10-25T11:07:19.657Z',
          __v: 0
        },
        {
          _id: '6176bb69451e58011e84cd8e',
          message: 'hello newbie',
          author_id: '617006a4f20e84e76bb11e78',
          published_date: '2021-10-25T14:12:57.111Z',
          __v: 0
        },
        {
          _id: '6176bbac451e58011e84cd90',
          message: 'hello newbie 2',
          author_id: '617006a4f20e84e76bb11e78',
          published_date: '2021-10-25T14:14:04.379Z',
          __v: 0
        },
        {
          _id: '6177b8c2c3c969958179e845',
          message: 'test',
          author_id: '617006a4f20e84e76bb11e78',
          published_date: '2021-10-26T08:13:54.949Z',
          __v: 0
        }
      ])
    )
  })

]
