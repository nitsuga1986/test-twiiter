import { render } from '@testing-library/react'
import React from 'react'

import TweetThemeProvider from '@/ui/style-design/ThemeProvider'

import { AppContext, INITIAL_STATE } from '../contexts/app.context'


export const backgroundColor = (element: Element): string => window.getComputedStyle(element).backgroundColor


export const renderWithThemeProviders = (children?: React.ReactNode): any => {
  return render(
    <TweetThemeProvider>
      <AppContext.Provider value={INITIAL_STATE}>{children}</AppContext.Provider>
    </TweetThemeProvider>
  )
}
