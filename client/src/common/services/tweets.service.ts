export const getTweets = async ():Promise<any> => {
  const API_URL:string = process.env.VITE_SSR_API_HOST as string

  const URL = `${API_URL}api/tweets`
  const response = await fetch(URL)
  return response.json()
}

// eslint-disable-next-line camelcase
export const addTweet = async (message:string, author_id:string):Promise<any> => {
  const API_URL:string = process.env.VITE_SSR_API_HOST as string
  const BODY = {
    message,
    author_id,
    published_date: new Date()
  }
  const URL = `${API_URL}api/tweets`
  const response = await fetch(URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(BODY)
  })
  return response.json()
}
