export const getUsers = async ():Promise<any> => {
  const API_URL:string = process.env.VITE_SSR_API_HOST as string

  const URL = `${API_URL}api/users`
  const response = await fetch(URL)
  return response.json()
}


export const followUser = async (id:string):Promise<any> => {
  const API_URL:string = process.env.VITE_SSR_API_HOST as string

  const URL = `${API_URL}api/users/follow/${id}`
  const response = await fetch(URL, { method: 'PUT' })
  return response.json()
}

export const unfollowUser = async (id:string):Promise<any> => {
  const API_URL:string = process.env.VITE_SSR_API_HOST as string

  const URL = `${API_URL}api/users/unfollow/${id}`
  const response = await fetch(URL, { method: 'PUT' })
  return response.json()
}
