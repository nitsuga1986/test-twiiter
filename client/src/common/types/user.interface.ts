export interface IUser {
  _id:string
  name:string
  hashtag:string
  following:boolean
  picture:string
}
