import React, { createContext, FC, ReactNode, useEffect, useState } from 'react'

import { getTweets } from '../services/tweets.service'
import { getUsers } from '../services/users.service'
import { ITweet } from '../types/tweet.interface'
import { IUser } from '../types/user.interface'

export interface ContextType {
  tweets: ITweet[]
  users: IUser[]
  selectedUser?: IUser
  setSelectedUser: (user: IUser | undefined) => void
  setTweets: (tweet: ITweet) => void
  setUser: (user: IUser) => void
}

export const INITIAL_STATE: ContextType = {
  tweets: [],
  users: [],
  selectedUser: undefined,
  setSelectedUser: () => { },
  setTweets: () => { },
  setUser: () => { }
}


export const AppContext = createContext<ContextType>(INITIAL_STATE)


const AppProvider: FC<ReactNode> = ({ children }) => {
  const [state, setState] = useState<ContextType>(INITIAL_STATE)

  useEffect(() => {
    const getData = async (): Promise<void> => {
      const users = await getUsers()
      const tweets = await getTweets()

      setState({
        ...state,
        users,
        tweets
      })
    }
    getData()
  }, [])

  const setTweets = (tweet: ITweet): void => {
    setState({ ...state, tweets: [...state.tweets, tweet] })
  }

  const setUser = (user: IUser): void => {
    const clonedUsers = [...state.users]
    const users = clonedUsers.map((iterator: IUser) => {
      if (iterator._id === user._id) {
        return { ...user }
      }
      return { ...iterator }
    })
    setState({ ...state, users })
  }

  const setSelectedUser = async (selected: IUser | undefined): Promise<void> => {
    const originalTweets = await getTweets()
    let tweetsSelected = originalTweets
    if (selected) {
      tweetsSelected = originalTweets.filter((tweet: ITweet) => tweet.author_id === selected._id)
    }

    setState({ ...state, selectedUser: selected, tweets: tweetsSelected })
  }
  return (
    <AppContext.Provider value={{ tweets: state.tweets, users: state.users, selectedUser: state.selectedUser, setTweets, setUser, setSelectedUser }}>
      {children}
    </AppContext.Provider>
  )
}

export default AppProvider
