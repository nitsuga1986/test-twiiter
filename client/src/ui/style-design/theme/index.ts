import darkTheme from './dark'
import type { Theme } from './light'
import lightTheme from './light'

const themeModes = {
  dark: darkTheme,
  light: lightTheme
}

export type { Theme }
export type ThemeModes = keyof typeof themeModes
export const themes = themeModes
