import lightTheme, { Theme } from '../light'

const darkTheme: Theme = lightTheme

export default darkTheme
