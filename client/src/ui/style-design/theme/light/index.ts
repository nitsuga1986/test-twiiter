import colors from './colors'
import global from './global'
import layout from './layout'
import spacing from './spacing'
import typography from './typography'

const lightTheme = {
  colors,
  typography,
  spacing,
  layout,
  global
}

export type Theme = typeof lightTheme
export default lightTheme
