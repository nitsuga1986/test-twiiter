const typography = {
  global: {
    family: 'Inter, sans-serif'
  },
  bodyRegular: {
    size: '0.875rem',
    weight: 400,
    lineHeight: '1.25rem'
  },
  bodyBold: {
    size: '0.875rem',
    weight: 700,
    lineHeight: '1.25rem'
  },
  labelRegular: {
    size: '1rem',
    weight: 500,
    lineHeight: '1.21rem'
  },
  captionRegular: {
    size: '0.75rem',
    weight: 400,
    lineHeight: '0.75rem'
  },
  captionBold: {
    size: '0.75rem',
    weight: 700,
    lineHeight: '1rem'
  },
  header: {
    h1: {
      size: '1.8125rem',
      lineHeight: '2.5rem',
      weight: 500
    },
    h2: {
      size: '1.5rem',
      lineHeight: '2rem',
      weight: 500
    },
    h3: {
      size: '1.25rem',
      lineHeight: '1.75rem',
      weight: 700
    },
    h4: {
      size: '1rem',
      lineHeight: '1.25rem',
      weight: 500
    },
    h5: {
      size: '0.875rem',
      lineHeight: '1.25rem',
      weight: 500
    },
    h6: {
      size: '0.75rem',
      lineHeight: '1rem',
      weight: 500
    }
  },
  table: {
    size: '1rem'
  }
}

export default typography
