const colors = {
  brand: 'rgb(16, 88, 126)',
  primary: {
    dark: '#202842',
    main: '#5A6979',
    light: 'rgb(169, 178, 186)'
  },
  secondary: {
    main: '#F9F9F9'
  },

  black: '#1D1D1D',
  white: '#FFFFFF',

  border: '#E7EEF2'

}

export default colors
