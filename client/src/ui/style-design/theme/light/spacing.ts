const spacing = {
  // Base 8 - the number in the name represents a multiple of 8 in px
  spacing0025: '0.125rem',
  spacing005: '0.25rem',
  spacing01: '0.5rem',
  spacing02: '1rem',
  spacing03: '1.5rem',
  spacing04: '2rem',
  spacing05: '2.5rem',
  spacing06: '3rem',
  spacing07: '3.5rem',
  spacing08: '4rem',
  spacing09: '4.5rem',
  spacing10: '5rem',
  spacing11: '5.5rem'
}

export default spacing
