import React, { FC } from 'react'
import { ThemeProvider, ThemeProviderProps } from 'styled-components'

import GlobalStyle from '../GlobalStyle'
import { Theme, ThemeModes, themes } from '../theme'

interface Props extends Omit<ThemeProviderProps<Theme>, 'theme'> {
  mode?: ThemeModes
}

const TweetThemeProvider: FC<Props> = ({ children, mode = 'light', ...props }): JSX.Element => {
  return (
    <ThemeProvider theme={themes[mode]} {...props}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  )
}

export default TweetThemeProvider
