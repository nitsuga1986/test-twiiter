import { css } from 'styled-components'

import typography from '../../theme/light/typography'

enum Typo {
  BODY_REGULAR = 'bodyRegular',
  BODY_BOLD = 'bodyBold',
  CAPTION_REGULAR = 'captionRegular',
  CAPTION_BOLD = 'captionBold',
}

enum HeaderTypo {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6',
}

const getTypoAttrs = (typo: Typo):Object => {
  return css`
    font-size: ${typography[typo].size};
    line-height: ${typography[typo].lineHeight};
    font-weight: ${typography[typo].weight};
  `
}

const getHeaderTypoAttrs = (typo: HeaderTypo):Object => css`
  font-size: ${typography.header[typo].size};
  line-height: ${typography.header[typo].lineHeight};
  font-weight: ${typography.header[typo].weight};
`

export const styledTypographyAttrs = {
  bodyRegular: getTypoAttrs(Typo.BODY_REGULAR),
  bodyBold: getTypoAttrs(Typo.BODY_BOLD),
  captionRegular: getTypoAttrs(Typo.CAPTION_REGULAR),
  captionBold: getTypoAttrs(Typo.CAPTION_BOLD),
  header: {
    h1: getHeaderTypoAttrs(HeaderTypo.H1),
    h2: getHeaderTypoAttrs(HeaderTypo.H2),
    h3: getHeaderTypoAttrs(HeaderTypo.H3),
    h4: getHeaderTypoAttrs(HeaderTypo.H4),
    h5: getHeaderTypoAttrs(HeaderTypo.H5),
    h6: getHeaderTypoAttrs(HeaderTypo.H6)
  }
}
