import styled from 'styled-components'


export const Main = styled.main`
  background:${(props):string => props.theme.colors.white};
  display:flex;
  justify-content: space-between;
  height: 100vh;
}
`
