import { Followers } from '@components/Followers'
import { Header } from '@components/Header'
import { Posts } from '@components/Posts'
import React, { useContext } from 'react'

import { AppContext } from '@/common/contexts/app.context'

import { Main } from './styles'

export const MainLayout = (): JSX.Element => {
  const { users, tweets } = useContext(AppContext)

  return (
    <Main>
      <Header users={users} />
      <Posts tweets={tweets} />
      <Followers users={users} />
    </Main>
  )
}
