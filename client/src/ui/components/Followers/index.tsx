import { Card } from '@components/Card'
import React from 'react'

import { IUser } from '@/common/types/user.interface'

import { Aside } from './styles'
interface IProps {
  users: IUser[]
}
const ID_USER_REGISTERED = process.env.VITE_ID_USER_REGISTERED

export const Followers = ({ users }: IProps): JSX.Element => {
  const ME = users.find((user: IUser) => user._id === ID_USER_REGISTERED)
  const following = users.filter((user: IUser) => user._id !== ME?._id && user.following)
  const unfollowing = users.filter((user: IUser) => user._id !== ME?._id && !user.following)

  return (
    <Aside>
      <Card following title="Following" users={following} />
      <Card title="Who to follow" users={unfollowing} />
    </Aside>
  )
}
