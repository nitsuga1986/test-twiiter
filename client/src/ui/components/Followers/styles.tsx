import styled from 'styled-components'

export const Aside = styled.aside`
  display:flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  background: ${(props): string => props.theme.colors.secondary.main};
  padding:1rem;
  border-radius:${(props): string => props.theme.global.borderRadius};
  gap:3rem;
  flex-grow:1;
  max-width:20rem;
`

