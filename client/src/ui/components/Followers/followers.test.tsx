import { getByRole, getByText } from '@testing-library/dom'
import React from 'react'

import { FAKE_USERS } from '@/common/testing/mocks/users'
import { renderWithThemeProviders } from '@/common/testing/utilities'

import { Followers } from './'

test('Should render the followers and  show unfollow button', () => {
  const { container } = renderWithThemeProviders(<Followers users={FAKE_USERS} />)
  const USER_MICHAEL = getByText(container, 'Michael Scott')
  getByRole(USER_MICHAEL, 'button', { name: 'unfollow' })
})




