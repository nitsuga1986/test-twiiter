import { fireEvent, getByRole, getByText } from '@testing-library/dom'
import React from 'react'

import { FAKE_USERS } from '@/common/testing/mocks/users'
import { renderWithThemeProviders } from '@/common/testing/utilities'
import { IUser } from '@/common/types/user.interface'

import { Card } from './'

const ID_USER_REGISTERED = process.env.VITE_ID_USER_REGISTERED
const ME = FAKE_USERS.find((user: IUser) => user._id === ID_USER_REGISTERED)

test('Should render a Card with unfollow request', () => {
  const TITLE = 'Following'
  const following = FAKE_USERS.filter((user: IUser) => user._id !== ME?._id && user.following)
  const { container } = renderWithThemeProviders(<Card title={TITLE} users={following} following />)

  const USER_MICHAEL = getByText(container, 'Michael Scott')
  const unfollowButton = getByRole(USER_MICHAEL, 'button', { name: 'unfollow' })

  fireEvent.click(unfollowButton)
})

test('Should render a Card with follow request', () => {
  const TITLE = 'Who to follow'
  const unfollowing = FAKE_USERS.filter((user: IUser) => user._id !== ME?._id && !user.following)
  const { container } = renderWithThemeProviders(<Card title={TITLE} users={unfollowing} />)

  getByText(container, 'Dwight Schrute')
  const USER_DWIGHT = getByText(container, 'Dwight Schrute')
  const followButton = getByRole(USER_DWIGHT, 'button', { name: 'follow' })

  fireEvent.click(followButton)
})

test('Should click over the user picture to get his tweets', () => {
  const TITLE = 'Following'
  const following = FAKE_USERS.filter((user: IUser) => user._id !== ME?._id && user.following)
  const { container } = renderWithThemeProviders(<Card title={TITLE} users={following} following />)
  const USER_MICHAEL = getByText(container, 'Michael Scott')
  const picture = getByRole(USER_MICHAEL, 'img')
  fireEvent.click(picture)
})






