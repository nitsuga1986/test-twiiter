import styled from 'styled-components'


export const Wrapper = styled.div<{ title: string }>`
  background:${(props): string => props.theme.colors.primary.dark};
  color:${(props): string => props.theme.colors.primary.light};
  border-radius:${(props): string => props.theme.global.borderRadius};
  padding:1rem 2rem;

  &::before {
    content: "${(props): string => props.title}";
    color:white;
    font-weight:${(props): string => props.theme.typography.header.h3.lineHeight};
    font-size: ${(props): string => props.theme.typography.header.h3.size};
    padding-bottom:1rem;
    display:block;
  }

  ul {
    list-style:none;
    margin:0;
    padding:0;
    li {
      display:flex;
      justify-content:space-between;
      align-items:center;
      gap:1rem;
      button {
        border-radius:${(props): string => props.theme.global.borderRadius};
        border:1px solid transparent;
        cursor:pointer;
      }
    }
  }
`

export const Picture = styled.img` 
  width:3rem;
  height:3rem;
  border-radius:50%;
  background-size:cover;
  cursor:pointer;
`
