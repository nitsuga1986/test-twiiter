import React, { useContext } from 'react'

import { AppContext } from '@/common/contexts/app.context'
import { followUser, unfollowUser } from '@/common/services/users.service'
import { IUser } from '@/common/types/user.interface'

import { Picture, Wrapper } from './styles'

interface IProps {
  title: string
  users: IUser[]
  following?: boolean
}

export const Card = ({ title, users, following = false }: IProps): JSX.Element => {
  const { setUser, setSelectedUser } = useContext(AppContext)

  const handleClick = async (user: IUser): Promise<void> => {
    if (following) {
      await unfollowUser(user._id).then(data => {
        data.ok && setUser({ ...user, following: false })
      })
    } else {
      await followUser(user._id).then(data => {
        data.ok && setUser({ ...user, following: true })
      })
    }
  }

  const getTweetsFromUser = (user: IUser): void => {
    following ? setSelectedUser(user) : window.alert(`You are not following ${user.name}`)
  }


  return (
    <Wrapper title={title}>
      {users.length === 0
        ? (<span>Not  users </span>)
        : (
          <ul>
            {users.map((user: IUser) => (
              <li key={user._id}>
                <Picture src={user.picture} onClick={(): void => getTweetsFromUser(user)} />
                {user.name}
                <button onClick={(): Promise<void> => handleClick(user)}>
                  {following ? 'unfollow' : 'follow'}
                </button>
              </li>
            )
            )}
          </ul>
        )}

    </Wrapper>
  )
}
