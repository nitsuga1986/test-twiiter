import React, { useContext } from 'react'

import { AppContext } from '@/common/contexts/app.context'
import { addTweet } from '@/common/services/tweets.service'
import { ITweet } from '@/common/types/tweet.interface'
import { IUser } from '@/common/types/user.interface'
import { fromNow } from '@/common/utilities'

import { AddPost } from '../AddPost'
import { getFollowersTweets } from './logic'
import { ContainerTweets, Content, Message, Name, NoTweets, Picture, Section, Tweet, TweetsSelected } from './styles'

const ID_USER_REGISTERED: string = process.env.VITE_ID_USER_REGISTERED as string

interface IProps {
  tweets: ITweet[]
}

const getUserInfo = (userId: string, users: IUser[]): IUser | undefined => {
  return users.find((user: IUser) => {
    return user._id === userId
  })
}

export const Posts = ({ tweets }: IProps): JSX.Element => {
  const { users, setSelectedUser, selectedUser, setTweets } = useContext(AppContext)
  const myTweets = getFollowersTweets(tweets, users)

  const handleAddTweet = async (value: string): Promise<void> => {
    if (value !== '') {
      const response = await addTweet(value, ID_USER_REGISTERED)
      if (response.data) {
        setTweets(response.data)
      }
    }
  }
  return (
    <Section>
      <AddPost onSendTweet={(value: string): Promise<void> => handleAddTweet(value)} />
      {selectedUser && <TweetsSelected>You are viewing {selectedUser.name} tweets <button onClick={(): void => setSelectedUser(undefined)}>Reset</button></TweetsSelected>}
      <ContainerTweets>

        {myTweets.length === 0 && <NoTweets>No tweets found</NoTweets>}
        {myTweets.map((tweet: ITweet) => {
          const user = getUserInfo(tweet.author_id, users)
          return (
            <Tweet key={tweet._id}>
              <Picture src={user?.picture} />
              <Content>
                {user && (
                  <Name onClick={(): void => setSelectedUser(user)}>
                    {user.name}
                    <i>{user.hashtag}</i>
                  </Name>)}
                <div>{fromNow(tweet.published_date)}</div>
                <Message>
                  {tweet.message}
                </Message>
              </Content>
            </Tweet>

          )
        })}
      </ContainerTweets>
    </Section>
  )
}
