
import { fireEvent, screen } from '@testing-library/dom'
import React from 'react'

import { FAKE_TWEETS } from '@/common/testing/mocks/tweets'
import { renderWithThemeProviders } from '@/common/testing/utilities'

import { Posts } from './'


test('Should render a list of post', () => {
  const { container } = renderWithThemeProviders(<Posts tweets={FAKE_TWEETS} />)
  const button = screen.getByRole('button')
  const textarea = container.querySelector('textarea')
  fireEvent.change(textarea, { target: { value: 'EXAMPLE_TEST' } })

  fireEvent.click(button)
})






