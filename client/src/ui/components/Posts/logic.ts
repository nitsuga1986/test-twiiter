import { ITweet } from '@/common/types/tweet.interface'
import { IUser } from '@/common/types/user.interface'

export const getFollowersTweets = (tweets:ITweet[], users: IUser[]):ITweet[] => {
  const followers = users.filter((user:IUser) => user.following).map((user:IUser) => user._id)
  const tweetsFollowers = tweets.filter((tweet:ITweet) => followers.includes(tweet.author_id))

  const reverseTweets = [...tweetsFollowers].reverse()
  return reverseTweets
}
