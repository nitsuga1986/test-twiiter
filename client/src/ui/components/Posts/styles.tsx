import styled from 'styled-components'

export const Section = styled.section`
  flex-grow:4;
`
export const Tweet = styled.div`
  border: 1px solid ${(props): string => props.theme.colors.border};
  padding: 1rem;
  border-radius: ${(props): string => props.theme.global.borderRadius};
  margin: 1rem;
  justify-content: space-between;
  display:flex;
  gap:0.5rem;
  max-width: ${(props): string => props.theme.layout.content.width};
  margin: 1rem auto;
`

export const Picture = styled.img`
  width:4rem;
  height:4rem;
  border-radius:50%;
`

export const Content = styled.div`
  display:inline-flex;
  flex-direction:column;
  justify-content:space-between;
  flex-wrap:wrap;
  flex-direction:row;
  flex:1;
`

export const Name = styled.span`
  font-size:${(props): string => props.theme.typography.bodyBold.size};
  font-weight:${(props): string => props.theme.typography.bodyBold.weight};
  color:${(props): string => props.theme.colors.primary.dark};
  i {
    font-weight:normal;
    font-style:normal;
    font-size:${(props): string => props.theme.typography.captionRegular.size};
    margin-left:0.25rem;
    color:${(props): string => props.theme.colors.primary.main};
  }

  &:hover {
    cursor:pointer;
  }
`


export const Message = styled.div`
  width:100%;
  font-size:${(props): string => props.theme.typography.bodyRegular.size};
  font-weight:${(props): string => props.theme.typography.bodyRegular.weight};
`

export const ContainerTweets = styled.div`
  height: 70vh;
  overflow: auto;
`

export const NoTweets = styled.h1`
  text-align:center;
`

export const TweetsSelected = styled.h4`
  text-align:center;
`

