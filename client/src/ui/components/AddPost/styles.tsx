import styled from 'styled-components'

export const Wrapper = styled.div`
  max-width: ${(props): string => props.theme.layout.content.width};
  margin:0 auto;
  margin: 1rem auto;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  background:${(props): string => props.theme.colors.border};
  align-items:end;

  textarea {
    width:100%;
    resize:none;
    height:6rem;
  }
  button {
    padding: 1rem;
    margin: 1rem 0;
    background: white;
    border: 0;
    border-radius: 4px;
    cursor: pointer;

  }

`
