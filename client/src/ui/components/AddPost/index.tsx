import React, { useContext, useState } from 'react'

import { AppContext } from '@/common/contexts/app.context'
import { addTweet } from '@/common/services/tweets.service'

import { Wrapper } from './styles'


interface IProps {
  onSendTweet: (value: string) => void
}

export const AddPost = ({ onSendTweet }: IProps): JSX.Element => {
  const [value, setValue] = useState('')

  const handleSendTweet = (): void => {
    onSendTweet(value)
    setValue('')
  }
  return (
    <Wrapper>
      <textarea value={value} onChange={((event): void => setValue(event.target.value))} />
      <button onClick={handleSendTweet}>send tweet</button>
    </Wrapper>
  )
}
