import { fireEvent } from '@testing-library/dom'
import React from 'react'

import { renderWithThemeProviders } from '@/common/testing/utilities'

import { AddPost } from './'

test('Should render a textarea to post', () => {
  const mockFn = jest.fn()
  const EXAMPLE_TEST = 'hello'
  const { container } = renderWithThemeProviders(<AddPost onSendTweet={mockFn} />)
  const textarea = container.querySelector('textarea')
  fireEvent.change(textarea, { target: { value: EXAMPLE_TEST } })
  expect(textarea.value).toBe(EXAMPLE_TEST)
})

test('Should send the value', () => {
  const mockFn = jest.fn()
  const EXAMPLE_TEST = 'hello'

  const { container } = renderWithThemeProviders(<AddPost onSendTweet={mockFn} />)
  const textarea = container.querySelector('textarea')
  const button = container.querySelector('button')
  fireEvent.change(textarea, { target: { value: EXAMPLE_TEST } })
  fireEvent.click(button)

  expect(mockFn).toHaveBeenCalledTimes(1)
  expect(mockFn).toHaveBeenCalledWith(EXAMPLE_TEST)
})
