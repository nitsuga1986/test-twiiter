import React, { useContext } from 'react'

import { AppContext } from '@/common/contexts/app.context'
import { IUser } from '@/common/types/user.interface'

import { Card, Container, Followers, Hash, Name, Picture, Wrapper } from './styles'

interface IProps {
  users: IUser[]
}

const ID_USER_REGISTERED = process.env.VITE_ID_USER_REGISTERED

export const Header = ({ users }: IProps): JSX.Element => {
  const { setSelectedUser } = useContext(AppContext)

  const ME: IUser | undefined = users.find((user: IUser) => user._id === ID_USER_REGISTERED)

  const following = users.filter((user: IUser) => user._id !== ME?._id && user.following)
  const unfollowing = users.filter((user: IUser) => user._id !== ME?._id && !user.following)
  return (
    <Wrapper>
      {ME && (
        <Container data-testid="container-header" onClick={(): void => setSelectedUser(ME)}>
          <Picture src={ME?.picture} />
          <Name>{ME?.name}</Name>
          <Hash>{ME?.hashtag}</Hash>
          <Followers>
            <Card data-testid="followers">
              <span>{following.length}</span> following
            </Card>
            <Card data-testid="unfollowers">
              <span>{unfollowing.length}</span> to follow
            </Card>
          </Followers>
        </Container>
      )}
    </Wrapper>
  )
}
