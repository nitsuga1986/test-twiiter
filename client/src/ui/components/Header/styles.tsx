import styled from 'styled-components'


export const Wrapper = styled.div`
  display:flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  background: ${(props): string => props.theme.colors.secondary.main};
  padding:1rem;
  border-radius:${(props): string => props.theme.global.borderRadius};
  gap:1rem;
  flex-grow:1;
`

export const Name = styled.h1`
  color: ${(props): string => props.theme.colors.black};
`

export const Hash = styled.label`
  color: ${(props): string => props.theme.colors.primary.dark};
`

export const Picture = styled.img`
  border-radius:50%;
  margin:2rem 0;
`
export const Followers = styled.div`
  display:flex;
  gap:1rem;
`
export const Card = styled.div`
  display:inline-block;
  span {
    font-weight:700;
  }
`

export const Container = styled.div`
&:hover {
  cursor:pointer;
}`

