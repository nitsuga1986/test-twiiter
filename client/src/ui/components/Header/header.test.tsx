import { fireEvent, getByTestId, getByText, screen } from '@testing-library/dom'
import React from 'react'

import { FAKE_USERS } from '@/common/testing/mocks/users'
import { renderWithThemeProviders } from '@/common/testing/utilities'

import { Header } from './'

test('Should render header with the current user', () => {
  const { container } = renderWithThemeProviders(<Header users={FAKE_USERS} />)
  const image = container.querySelector('img')
  const containerHeader = screen.getByTestId('container-header')
  fireEvent.click(containerHeader)
  expect(image).toBeInTheDocument()
})


test('Should render header with the current user', () => {
  const { container } = renderWithThemeProviders(<Header users={FAKE_USERS} />)

  screen.getByRole('heading', { name: /Agustin Herrera/i })
  getByText(container, /@nitsuga1986/i)
  expect(container.querySelector('img').getAttribute('src')).toBe('https://pbs.twimg.com/profile_images/1084389757633863680/40PiFZOJ_reasonably_small.jpg')

  const followers = getByTestId(container, 'followers')
  getByText(followers, '2')

  const unfollowers = getByTestId(container, 'unfollowers')
  getByText(unfollowers, '1')
})


