/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '\\.(svg)$': '<rootDir>/src/__mocks__/fileMock.ts',
    '\\.(css|less)$': '<rootDir>/src/__mocks__/styleMock.ts',
    '^@components/(.*)$': '<rootDir>/src/ui/components/$1',
    '^@/common/(.*)$': '<rootDir>/src/common/$1',
    '^@/ui/(.*)$': '<rootDir>/src/ui/$1'

  },
  setupFilesAfterEnv: ['<rootDir>/src/jest-setup.ts'],
  collectCoverageFrom: ['src/**/*.tsx', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  testMatch: ['**/*.test.tsx', '**/*.test.ts'],
  collectCoverage: true,
  transform: {
    '^.+\\.(t|j)sx?$': ['@swc-node/jest']
  }
}

const arr = require('fs')
  .readFileSync('.env', 'utf8')
  .split('\n')
  .reduce((vars, i) => {
    const [variable, value] = i.split('=')
    vars[variable] = value
    return vars
  }, {})

process.env = Object.assign(process.env, arr)
