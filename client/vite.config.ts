import reactRefresh from '@vitejs/plugin-react-refresh'
import { defineConfig, loadEnv } from 'vite'


export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd())

  // expose .env as process.env instead of import.meta since jest does not import meta yet
  const envWithProcessPrefix = Object.entries(env).reduce(
    (prev, [key, val]) => {
      return {
        ...prev,
        ['process.env.' + key]: `"${val}"`
      }
    },
    {}
  )

  return {
    define: envWithProcessPrefix,
    server: {
      host: '0.0.0.0',
      port: 3000
    },
    plugins: [
      reactRefresh()

    ],
    publicDir: 'public',
    resolve: {
      alias: [
        { find: '@', replacement: '/src' },
        { find: '@components', replacement: '/src/ui/components' },
        { find: '@style-design', replacement: '/src/ui/style-design' }
      ]
    }
  }
})
