const mongoose = require('mongoose');

const TweetSchema = new mongoose.Schema({
  message: {
    type: String,
    required: true
  },
  author_id: {
    type: String,
    required: true
  },
  published_date: {
    type: Date,
    required: true
  },
});

module.exports = Tweet = mongoose.model('tweet', TweetSchema);