const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  following: {
    type: Boolean,
    required: true
  },
  hashtag: {
    type: String,
    required: true
  },
  picture: {
    type: String,
    required: true
  },
});

module.exports = User = mongoose.model('user', UserSchema);