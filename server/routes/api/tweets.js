const express = require('express');
const router = express.Router();


const Tweet = require('../../models/Tweet');


// @route GET api/tweets
// @description Get all tweets
// @access Public
router.get('/', (req, res) => {
  Tweet.find().sort('published_date')
    .then(tweets => res.json(tweets))
    .catch(err => res.status(404).json({ message: 'No Tweets found' }));
});

// @route GET api/tweets/:user_id
// @description Get all tweets from user
// @access Public
router.get('/:user_id', (req, res) => {
  Tweet.find({ author: req.params.user_id })
    .then(tweets => res.json(tweets))
    .catch(err => res.status(404).json({ message: 'No Tweets found' }));
});


// @route GET api/tweets
// @description add/save tweet
// @access Public
router.post('/', (req, res) => {
  Tweet.create(req.body)
    .then(tweet => res.json({ data: tweet, message: "Tweet added succesfully" }))
    .catch(err => res.status(400).json({ error: err, message: 'Unable to add this tweet' }));
});



module.exports = router;