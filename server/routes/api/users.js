const express = require('express');
const router = express.Router();


const User = require('../../models/User');

// @route GET api/users
// @description Get all users
// @access Public
router.get('/', (req, res) => {
  User.find()
    .then(users => res.json(users))
    .catch(err => res.status(404).json({ message: 'No Users found' }));
});


// @route GET api/users
// @description add/save user
// @access Public
router.post('/', (req, res) => {
  User.create(req.body)
    .then(user => res.json({ ok: true, data: user, message: "User added succesfully" }))
    .catch(err => res.status(400).json({ ok: false, error: err, message: 'Unable to add this user' }));
});

// // @route GET api/users/unfollow/:id
// // @description Unfollow the user
// // @access Public
router.put('/unfollow/:id', (req, res) => {
  User.findByIdAndUpdate(req.params.id, { following: false })
    .then(user => res.json({ ok: true, msg: 'Updated successfully', data: user }))
    .catch(err =>
      res.status(400).json({ ok: false, error: err, message: 'Unable to update the User' })
    );
});

// // @route GET api/users/follow/:id
// // @description Follow the user
// // @access Public
router.put('/follow/:id', (req, res) => {
  User.findByIdAndUpdate(req.params.id, { following: true })
    .then(user => res.json({ ok: true, msg: 'Updated successfully', data: user }))
    .catch(err =>
      res.status(400).json({ ok: false, error: err, message: 'Unable to update the User' })
    );
});


module.exports = router;