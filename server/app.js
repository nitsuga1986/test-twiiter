const express = require('express');
var cors = require('cors');

const connectDB = require('./config/db');
const tweets = require('./routes/api/tweets');
const users = require('./routes/api/users');


const app = express();

// Connect Database
connectDB();

app.use(cors({ origin: true, credentials: true }));

app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('Hello world!'));
app.use('/api/tweets', tweets);
app.use('/api/users', users);

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));