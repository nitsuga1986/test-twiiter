#  Twitter [Agustin Herrera]

This is a test for a position as Frontend 

## Description

Implement a web-based social networking application (similar to Twitter) 


## Getting Started

In this project there are two folder:

- server: contains all related the server in Node with mongoose for the API
- client: contains all related the frontend client

### Installing

* Go to server folder and install all dependencies

```
npm install
```

* Go to client folder and install all dependencies

```
npm install
```


### Executing program

* Once all the dependencies have been installed, go to server folder and execute:
```
npm run start
```
This init the server.

* Same way, go to client folder and execute:
```
npm run dev
```
This will open a port (3000) and you could check the website. Navigate to http://localhost:3000


## Testing

* To execute the testing, go to client folder and execute:
```
npm run test
```

## Author


Agustin Herrera
[nitsuga1986@gmail.com](mail)

